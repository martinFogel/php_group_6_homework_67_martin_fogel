@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            @foreach($words as $word)
                <div class="col" style="padding: 35px 0 0 0;" id="delete-article-{{$word->id}}">
                    <div class="card" style="width: 18rem;">
                        <div class="card-header">
                            <a href="{{route('words.show', ['word' => $word])}}"
                               class="card-link">{{$word->translate('ru')->word}}</a>
                            <br>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="container">
        <h1 class="m-5">@lang('messages.order')</h1>
        <form method="post" action="{{ route('words.store') }}">
            @csrf
            <div class="form-group">
                <label for="ru">@lang('messages.name')</label>
                <input type="text" class="form-control @error('ru') is-invalid @enderror" id="ru" name="ru"
                       value="word"/>
                @error('ru')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">@lang('messages.order_translation')</button>
        </form>
    </div>
@endsection
