@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-3">
                <form method="post" action="{{ route('words.update', ['word' => $word])}}">
                    @method('put')
                    @csrf
                    @foreach($word->translations as $translation)
                        <div class="col" style="padding: 35px 0 0 0;">
                            <div class="card" style="width: 18rem;">
                                <div class="card-header">
                                    @if($translation->locale == 'ru')
                                        <p>@lang('languages.russian')</p>
                                    @endif
                                    @if($translation->locale == 'en')
                                        <p>@lang('languages.english')</p>
                                    @endif
                                    @if($translation->locale == 'gr')
                                        <p>@lang('languages.greece')</p>
                                    @endif
                                    @if($translation->locale == 'de')
                                        <p>@lang('languages.german')</p>
                                    @endif
                                    @if($translation->locale == 'fr')
                                        <p>@lang('languages.french')</p>
                                    @endif
                                    @if($translation->word != '')
                                        <p>{{$translation->word}}</p>
                                    @else
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control"
                                                   id="{{$translation->locale}}" name="{{$translation->locale}}"
                                                   value="{{$translation->word}}"/>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <br>
                    <button type="submit" class="btn btn-primary">@lang('messages.save_translations')</button>
                </form>
            </div>
        </div>
    </div>
@endsection
