<?php
return [
    'words' => 'слова',
    'order'  => 'Заказать слово для перевода',
    'name' => 'Фраза',
    'translator' => 'Переводчик',
    'order_translation' => 'Заказать перевод',
    'save_translations' => 'Сохранить переводы'
];
