<?php
return [
    'words' => 'words',
    'order' => 'Create order for word',
    'name'  => 'Phrase',
    'translator' => 'Translator',
    'order_translation' => 'Order translation',
    'save_translations' => 'Save translations'
];
