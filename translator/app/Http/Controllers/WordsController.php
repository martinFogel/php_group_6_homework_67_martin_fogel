<?php

namespace App\Http\Controllers;

use App\Models\Word;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class WordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $words = Word::all();
        return view('words.index', compact('words'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $data = ['user_id' => auth()->user()->id, 'en' => ['word' => ''], 'ru' => ['word' => $request->input('ru')],
            'fr' => ['word' => ''], 'de' => ['word' => ''], 'gr' => ['word' => '']];
        $word = Word::create($data);
        $word->save();
        return redirect()->route('words.index')->with('status', "{$word->word} successfully created!");
    }

    /**
     * Display the specified resource.
     *
     * @param Word $word
     * @return Application|Factory|View
     */
    public function show(word $word)
    {
        return view('words.show', compact('word'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Word $word
     * @return RedirectResponse
     */
    public function update(Request $request, Word $word)
    {
        $data = [
            'en' => ['word' => $request->input('en') ? $request->input('en') : ''],
            'fr' => ['word' => $request->input('fr') ? $request->input('fr') : ''],
            'de' => ['word' => $request->input('de') ? $request->input('de') : ''],
            'gr' => ['word' => $request->input('gr') ? $request->input('gr') : ''],
        ];
        if ($data['en']['word'] == '') {
            foreach ($word->translations as $translation) {
                if ($translation->locale == 'en') {
                    $data['en'] = ['word' => $translation->word];
                }
            }
        } if ($data['fr']['word'] == '') {
            foreach ($word->translations as $translation) {
                if ($translation->locale == 'fr') {
                    $data['fr'] = ['word' => $translation->word];
                }
            }
        } if ($data['de']['word'] == '') {
            foreach ($word->translations as $translation) {
                if ($translation->locale == 'de') {
                    $data['de'] = ['word' => $translation->word];
                }
            }
        } if ($data['gr']['word'] == '') {
            foreach ($word->translations as $translation) {
                if ($translation->locale == 'gr') {
                    $data['gr'] = ['word' => $translation->word];
                }
            }
        }
        $word->update($data);
        return back();
    }
}
