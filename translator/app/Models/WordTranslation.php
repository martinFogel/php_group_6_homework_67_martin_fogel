<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WordTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['word'];

    /**
     * @return BelongsTo
     */
    public function phrase(): BelongsTo
    {
        return $this->belongsTo(Word::class);
    }
}
